uint32_t
_VISCA_write_packet_data(VISCAInterface_t *iface, VISCACamera_t *camera, VISCAPacket_t *packet)
{
  DWORD iBytesWritten;
  WriteFile(iface->port_fd, packet->bytes, packet->length, &iBytesWritten, NULL);
  if ( iBytesWritten < packet->length )
      return VISCA_FAILURE;
  else
      return VISCA_SUCCESS;
}


void
_VISCA_append_byte(VISCAPacket_t *packet, unsigned char byte)
{
  packet->bytes[packet->length]=byte;
  (packet->length)++;
}

uint32_t
_VISCA_send_packet(VISCAInterface_t *iface, VISCACamera_t *camera, VISCAPacket_t *packet)
{
  DWORD iBytesWritten;
  static long bytesTogether = 0;
  BOOL rVal = 0;
  DWORD errors;
  COMSTAT stat;
  int nTrials;

  // check data:
  if ((iface->address>7)||(camera->address>7)||(iface->broadcast>1))
    {
	  _RPTF3(_CRT_WARN,"Invalid header parameters %d %d %d\n",iface->address,camera->address,iface->broadcast);
	  return VISCA_FAILURE;
    }

  // build header:
  packet->bytes[0]=0x80;
  packet->bytes[0]|=(iface->address << 4);
  if (iface->broadcast>0)
    {
      packet->bytes[0]|=(iface->broadcast << 3);
      packet->bytes[0]&=0xF8;
    }
  else
    packet->bytes[0]|=camera->address;

  // append footer
  _VISCA_append_byte(packet,VISCA_TERMINATOR);

  for (nTrials = 0; nTrials < 3 && rVal == 0; nTrials++) {
    if (nTrials > 0)
      ClearCommError(iface->port_fd, &errors, &stat);
	rVal = WriteFile(iface->port_fd, &packet->bytes, packet->length, &iBytesWritten, NULL);
  }

  if ( iBytesWritten < packet->length )
    {
      DWORD lastError = GetLastError();
      return VISCA_FAILURE;
    }
  else {
    bytesTogether += iBytesWritten;
    return VISCA_SUCCESS;
  }
}

uint32_t
_VISCA_get_packet(VISCAInterface_t *iface)
{
  int pos=0;
  BOOL  rc;
  DWORD iBytesRead;

  // wait for message
  rc=ReadFile(iface->port_fd, iface->ibuf, 1, &iBytesRead, NULL);
  if ( !rc || iBytesRead==0 )
  {
      // Obtain the error code
      //m_lLastError = ::GetLastError();
	  _RPTF0(_CRT_WARN,"ReadFile failed.\n");
      return VISCA_FAILURE;
  }
  while (iface->ibuf[pos]!=VISCA_TERMINATOR) {
    if ( ++pos >= VISCA_INPUT_BUFFER_SIZE )
	{
        // Obtain the error code
        //m_lLastError = ::GetLastError();
  	    _RPTF0(_CRT_WARN,"illegal reply packet.\n");
        return VISCA_FAILURE;
    }
    rc=ReadFile(iface->port_fd, iface->ibuf + pos, 1, &iBytesRead, NULL);
	if ( !rc || iBytesRead==0 )
	{
        // Obtain the error code
        //m_lLastError = ::GetLastError();
	  _RPTF0(_CRT_WARN,"ReadFile failed.\n");
        return VISCA_FAILURE;
    }
  }
  iface->bytes=pos+1;

  return VISCA_SUCCESS;
}

void
_VISCA_init_packet(VISCAPacket_t *packet)
{
  // we start writing at byte 1, the first byte will be filled by the
  // packet sending function. This function will also append a terminator.
  packet->length=1;
}


uint32_t
_VISCA_get_reply(VISCAInterface_t *iface, VISCACamera_t *camera)
{
  // first message: -------------------
  if (_VISCA_get_packet(iface)!=VISCA_SUCCESS)
    return VISCA_FAILURE;
  iface->type=iface->ibuf[1]&0xF0;

  // skip ack messages
  while (iface->type==VISCA_RESPONSE_ACK)
    {
      if (_VISCA_get_packet(iface)!=VISCA_SUCCESS)
        return VISCA_FAILURE;
      iface->type=iface->ibuf[1]&0xF0;
    }

  switch (iface->type)
    {
    case VISCA_RESPONSE_CLEAR:
      return VISCA_SUCCESS;
      break;
    case VISCA_RESPONSE_ADDRESS:
      return VISCA_SUCCESS;
      break;
    case VISCA_RESPONSE_COMPLETED:
      return VISCA_SUCCESS;
      break;
    case VISCA_RESPONSE_ERROR:
      return VISCA_SUCCESS;
      break;
    }
  return VISCA_FAILURE;
}

uint32_t
_VISCA_send_packet_with_reply(VISCAInterface_t *iface, VISCACamera_t *camera, VISCAPacket_t *packet)
{
  if (_VISCA_send_packet(iface,camera,packet)!=VISCA_SUCCESS)
    return VISCA_FAILURE;

  if (_VISCA_get_reply(iface,camera)!=VISCA_SUCCESS)
    return VISCA_FAILURE;

  return VISCA_SUCCESS;
}

uint32_t
VISCA_open_serial(VISCAInterface_t *iface, const char *device_name)
{
  BOOL     m_bPortReady;
  HANDLE   m_hCom;
  DCB      m_dcb;
  COMMTIMEOUTS cto;

  m_hCom = CreateFile(device_name,
		      GENERIC_READ | GENERIC_WRITE,
		      0, // exclusive access
		      NULL, // no security
		      OPEN_EXISTING,
		      0, // no overlapped I/O
		      NULL); // null template

  // Check the returned handle for INVALID_HANDLE_VALUE and then set the buffer sizes.
  if (m_hCom == INVALID_HANDLE_VALUE)
  {
	_RPTF1(_CRT_WARN,"cannot open serial device %s\n",device_name);
	iface->port_fd = NULL;
    return VISCA_FAILURE;
  }

  m_bPortReady = SetupComm(m_hCom, 4, 4); // set buffer sizes

  // Port settings are specified in a Data Communication Block (DCB). The easiest way to initialize a DCB is to call GetCommState to fill in its default values, override the values that you want to change and then call SetCommState to set the values.
  m_bPortReady = GetCommState(m_hCom, &m_dcb);
  m_dcb.BaudRate = 9600;
  m_dcb.ByteSize = 8;
  m_dcb.Parity = NOPARITY;
  m_dcb.StopBits = ONESTOPBIT;
  m_dcb.fAbortOnError = TRUE;

  // =========================================
  // (jd) added to get a complete setup...
  m_dcb.fOutxCtsFlow = FALSE;		     // Disable CTS monitoring
  m_dcb.fOutxDsrFlow = FALSE;		     // Disable DSR monitoring
  m_dcb.fDtrControl = DTR_CONTROL_DISABLE;   // Disable DTR monitoring
  m_dcb.fOutX = FALSE;			     // Disable XON/XOFF for transmission
  m_dcb.fInX = FALSE;			     // Disable XON/XOFF for receiving
  m_dcb.fRtsControl = RTS_CONTROL_DISABLE;   // Disable RTS (Ready To Send)
  m_dcb.fBinary = TRUE;			     // muss immer "TRUE" sein!
  m_dcb.fErrorChar = FALSE;
  m_dcb.fNull = FALSE;
  // =========================================

  m_bPortReady = SetCommState(m_hCom, &m_dcb);


  // =========================================
  // (jd) set timeout
  if (!GetCommTimeouts(m_hCom,&cto))
  {
      // Obtain the error code
      //m_lLastError = ::GetLastError();
      _RPTF0(_CRT_WARN,"unable to obtain timeout information\n");
      CloseHandle(m_hCom);
      return VISCA_FAILURE;
  }
  cto.ReadIntervalTimeout = 100;		     /* 20ms would be good, but 100 are for usb-rs232 */
  cto.ReadTotalTimeoutConstant = 2000;	     /* 2s  */
  cto.ReadTotalTimeoutMultiplier = 50;	     /* 50ms for each char */
  cto.WriteTotalTimeoutMultiplier = 500;
  cto.WriteTotalTimeoutConstant = 1000;
  if (!SetCommTimeouts(m_hCom,&cto))
  {
      // Obtain the error code
      //m_lLastError = ::GetLastError();
      _RPTF0(_CRT_WARN,"unable to setup timeout information\n");
      CloseHandle(m_hCom);
      return VISCA_FAILURE;
  }
  // =========================================

  // If all of these API's were successful then the port is ready for use.
  iface->port_fd = m_hCom;
  iface->address = 0;

  return VISCA_SUCCESS;
}

uint32_t
VISCA_set_address(VISCAInterface_t *iface, int *camera_num)
{
  VISCAPacket_t packet;
  int backup;
  VISCACamera_t camera; /* dummy camera struct */

  camera.address=0;
  backup=iface->broadcast;

  _VISCA_init_packet(&packet);
  _VISCA_append_byte(&packet,0x30);
  _VISCA_append_byte(&packet,0x01);

  iface->broadcast=1;
  if (_VISCA_send_packet(iface, &camera, &packet)!=VISCA_SUCCESS)
    {
      iface->broadcast=backup;
      return VISCA_FAILURE;
    }
  else
    iface->broadcast=backup;

  if (_VISCA_get_reply(iface, &camera)!=VISCA_SUCCESS)
    return VISCA_FAILURE;
  else
    {
      /* We parse the message from the camera here  */
      /* We expect to receive 4*camera_num bytes,
         every packet should be 88 30 0x FF, x being
         the camera id+1. The number of cams will thus be
         ibuf[bytes-2]-1  */
      if ((iface->bytes & 0x3)!=0) /* check multiple of 4 */
	return VISCA_FAILURE;
      else
	{
	  *camera_num=iface->ibuf[iface->bytes-2]-1;
	  if ((*camera_num==0)||(*camera_num>7))
	    return VISCA_FAILURE;
	  else
	    return VISCA_SUCCESS;
	}
    }

}

uint32_t
VISCA_clear(VISCAInterface_t *iface, VISCACamera_t *camera)
{
  VISCAPacket_t packet;

  _VISCA_init_packet(&packet);
  _VISCA_append_byte(&packet,0x01);
  _VISCA_append_byte(&packet,0x00);
  _VISCA_append_byte(&packet,0x01);

  if (_VISCA_send_packet(iface, camera, &packet)!=VISCA_SUCCESS)
    return VISCA_FAILURE;
  else
    if (_VISCA_get_reply(iface, camera)!=VISCA_SUCCESS)
      return VISCA_FAILURE;
    else
      return VISCA_SUCCESS;
}

uint32_t
VISCA_usleep(uint32_t useconds)
{
  uint32_t microsecs = useconds / 1000;
  Sleep (microsecs);
  return 0;
}

uint32_t
VISCA_set_zoom_value(VISCAInterface_t *iface, VISCACamera_t *camera, uint32_t zoom)
{
  VISCAPacket_t packet;

  _VISCA_init_packet(&packet);
  _VISCA_append_byte(&packet, VISCA_COMMAND);
  _VISCA_append_byte(&packet, VISCA_CATEGORY_CAMERA1);
  _VISCA_append_byte(&packet, VISCA_ZOOM_VALUE);
  _VISCA_append_byte(&packet, (zoom & 0xF000) >> 12);
  _VISCA_append_byte(&packet, (zoom & 0x0F00) >>  8);
  _VISCA_append_byte(&packet, (zoom & 0x00F0) >>  4);
  _VISCA_append_byte(&packet, (zoom & 0x000F));

  return _VISCA_send_packet_with_reply(iface, camera, &packet);
}

uint32_t
VISCA_unread_bytes(VISCAInterface_t *iface, unsigned char *buffer, uint32_t *buffer_size)
{
  // TODO
  *buffer_size = 0;
  return VISCA_SUCCESS;
}

uint32_t
VISCA_close_serial(VISCAInterface_t *iface)
{
  if (iface->port_fd != NULL)
    {
      CloseHandle(iface->port_fd);
      iface->port_fd = NULL;
      return VISCA_SUCCESS;
    }
  else
    return VISCA_FAILURE;
}
