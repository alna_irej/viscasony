/*
 * VISCA(tm) Camera Control Library Test Program
 * Copyright (C) 2002 Damien Douxchamps
 *
 * Written by Damien Douxchamps <douxchamps@ieee.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "visca/libvisca.h"
#include <stdlib.h>
#include <stdio.h>
#include "lib_win32.c"

#define EVI_D30

int main(int argc, char **argv)
{


  VISCAInterface_t iface;
  VISCACamera_t camera;


  int camera_num;
  //uint8_t value;
  //uint16_t zoom;
  //int pan_pos, tilt_pos;


  if (argc<2)
    {
      fprintf(stderr,"%s usage: %s <serial port device>\n",argv[0],argv[0]);
      exit(1);
    }

  if (VISCA_open_serial(&iface, argv[1])!=VISCA_SUCCESS)
    {
      fprintf(stderr,"%s: unable to open serial device %s\n",argv[0],argv[1]);
      exit(1);
    }

  iface.broadcast=0;
  VISCA_set_address(&iface, &camera_num);
  camera.address=1;
  VISCA_clear(&iface, &camera);



  //VISCA_usleep(500000);
uint16_t zoom=31424/100*atoi(argv[2]);

//zoom = zoom*argv[2];
  if (VISCA_set_zoom_value(&iface, &camera, zoom)!=VISCA_SUCCESS)
    fprintf(stderr,"error setting zoom\n");



  //VISCA_set_shutter_value(&iface, &camera, 0x0D00);

  {
    unsigned char packet[3000];
    uint32_t buffer_size = 3000;
    if (VISCA_unread_bytes(&iface, packet, &buffer_size)!=VISCA_SUCCESS)
    {
      uint32_t i;
      fprintf(stderr, "ERROR: %u bytes not processed", buffer_size);
      for (i=0;i<buffer_size;i++)
        fprintf(stderr,"%2x ",packet[i]);
      fprintf(stderr,"\n");
    }
  }
  VISCA_close_serial(&iface);
  exit(0);
}
